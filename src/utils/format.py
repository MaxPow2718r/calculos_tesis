import re

import pandas as pd

from io import StringIO

def data_to_csv(file):
    comments = ""
    csv = ""
    with open(file) as data:
        while True:
            c = data.read(1)
            if c == "%":
                comment_line = True
            elif c == " ":
                if not comment_line and not blank:
                    csv = csv + ","
                if comment_line:
                    comments = comments + c
                blank = True
            elif c == "\n":
                if comment_line:
                    comment_line = False
                    comments = comments + c
                else:
                    csv = csv + c
            elif not c:
                break
            else:
                if not comment_line:
                    if c == "i":
                        c = "j"
                    csv = csv + c
                else:
                    comments = comments + c
                blank = False

    comments = comments.split("\n")[-2]
    comments = re.split("  +", comments)
    comments = [x.strip() for x in comments]
    return comments, csv

if __name__ == "__main__":
    files = [
        "data/chamber_no_polymer_all_ranges_in_air_low_points.txt",
        "data/chamber_all_polymers_full_range_in_air_low_points.txt",
    ]
    headers = []
    csv = []

    for file in files:
        headers.append(data_to_csv(file)[0])
        csv.append(data_to_csv(file)[1])

    dfs = []
    for header, data in zip(headers, csv):
        df = pd.read_csv(StringIO(data), sep=",", index_col=None, names=header)
        if len(header) < 5:
            new_header = headers[1][2]
            df.insert(2, new_header, len(df[header[0]]) * [0])

        dfs.append(df)

    df = pd.concat(dfs, axis=0)
    df.to_csv("data/all_data.csv", index=False)
