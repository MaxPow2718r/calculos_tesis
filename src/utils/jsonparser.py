import json

if __name__ == "__main__":
    # metals
    copper = {"Copper": {"epsilon_r": None, "mu_r": 1, "conductivity": 5.9e7}}
    gold = {"Gold": {"epsilon_r": None, "mu_r": 1, "conductivity": 4.11e7}}
    tin = {"Tin": {"epsilon_r": None, "mu_r": 1, "conductivity": 9.17e6}}
    aluminium = {"Aluminium": {"epsilon_r": None, "mu_r": 1, "conductivity": 3.77e7}}

    # substrate
    fp4 = {"FP4323": {"epsilon_r": 3.7, "mu_r": 1, "conductivity": 0}}
    silicon = {"Silicon": {"epsilon_r": 11.7, "mu_r": 1, "conductivity": 1e-12}}
    sio2 = {"SiO2": {"epsilon_r": 4.2, "mu_r": 1, "conductivity": 0}}

    # films
    PANI = {"PANI": {"epsilon_r": 1, "mu_r": None, "conductivity": 0.33}}
    PEDOT = {"PEDOT": {"epsilon_r": 1, "mu_r": None, "conductivity": 10}}
    PPy = {"PPy": {"epsilon_r": 0.279, "mu_r": None, "conductivity": 7.9}}

    metals = {"metals": [copper, gold, tin, aluminium]}
    subs = {"substrate": [fp4, silicon, sio2]}
    films = {"film": [PANI, PEDOT, PPy]}

    materials = {"materials": [metals, subs, films]}

    print(json.dumps(materials, sort_keys=True, indent=4))
