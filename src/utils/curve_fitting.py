import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import matplotlib as mpl
mpl.rcParams["figure.figsize"] = (14.8, 9.6)

from sklearn.linear_model import LinearRegression

# Extracted from a graph with https://apps.automeris.io/wpd/
df = pd.read_csv("extra/liu_2021/points.csv", index_col=None, names=["x", "y"])

# calculate polynomial
deg = 3
z = np.polyfit(df["x"], df["y"], deg)
f = np.poly1d(z)

# Prints the polynomial function to standard output
for i, val in enumerate(z):
    if i == deg:
        print("({:e})x^{}".format(val, deg - i))
    else:
        print("({:e})x^{}".format(val, deg - i), end='+')

# calculate new x's and y's
df_min = df["x"].min()
df_max = df["x"].max()
steps = 100
x_new = np.linspace(df_min, df_max, steps)
y_new = f(x_new)

# Plot

# Linear regression
reg = LinearRegression()
reg.fit(df[["x"]], df.y)

plt.scatter(df["x"], df["y"], color="red", label="Datos originales")
plt.plot(x_new, y_new, label="Aproximación polinomial Deg = {}".format(deg))

y_new = reg.predict(x_new.reshape(-1, 1))
plt.plot(
    x_new,
    y_new,
    label="Regresión lineal $R^2 = {:.2f}$".format(reg.score(df[["x"]], df.y)),
)

# plt.xlim(0, 101)
# plt.ylim(0, 35)
plt.title("Respuesta de una película de polímero PEDOT:PSS a diferentes concentraciones de amoniaco")
plt.xlabel("Concentración (ppm)")
plt.ylabel("Respuesta ($\mathrm{R_{NH_3}/R_{aire}}$)")
plt.legend()
plt.grid()
plt.tight_layout()
plt.show()
