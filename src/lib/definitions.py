mat_1 = "Material Switch 1 Conductores index"
mat_2 = "Material Switch 2 Substrato index"
mat_3 = "Material Switch 3 Film index"
freq = "freq (Hz)"
z_ref = "abs(1 / ec.Y11)"
adm = "Admittance, 11 component (S)"
# z_ref = "abs(ec.V0_1 / ec.I0_1)"

metals = ["Copper", "Tin", "Gold", "Aluminium"]
subs = ["FP4323", "Silicon", "SiO2"]
films = ["Sin Polímero", "PEDOT", "PANI", "PPy"]

# m = metal, s = substrate, f = film
# copper = 1, gold = 2, tin = 3, aluminium = 4
# FP4323 = 1, silicon = 2, SiO2 = 3
#        m  s    m  s    m  s
co   = [[1, 1], [1, 2], [1, 3]]
gold = [[2, 1], [2, 2], [2, 3]]
tin  = [[3, 1], [3, 2], [3, 3]]
al   = [[4, 1], [4, 2], [4, 3]]

combinations = [co, gold, tin, al]
