# math module to calculate the ecuations for impedance to compare with the
# results from the simulation.
import json
import itertools

import numpy as np
import scipy as sp
import pandas as pd
import matplotlib.pyplot as plt
from scipy.special import ellipk as eliK

class SlotLine:
    def __init__(self, f, metal, subs):
        """
        The SlotLine class conglomerates the maths to compute a waveguide
        principal parameters. It's ideal, but not necessary, to use this method
        with the Sensor class.

        Arguments:
        f -- (int or np.array) The frequency or frequency range
        metal -- (dictionary) A dictionary with the metals parameters
        subs -- (dictionary) A dictionary with the substrate parameters
        """
        # Variables
        # Dimensions
        self.l = 6000e-6  # Substrate length
        self.swidth = 3118e-6  # Substrate width
        self.s = 1500e-6  # Substrate thickness
        self.w = 541e-6  # Line width
        self.a = 2036e-6  # Distance between two lines
        self.h = 72e-6  # Line thickness

        # permittivity and permeability of free space
        self.ε_o = 8.85418782e-12
        self.μ_o = 1.25663706212e-6

        # for air
        self.ε_a = 1.0006

        self.ω = 2 * np.pi * f

        for key, value in metal.items():
            self.σ = value["conductivity"]
            self.μ = self.μ_o * value["mu_r"]

        for key, value in subs.items():
            self.ε_sus = self.ε_o * value["epsilon_r"]

        # Eliptical integrals components
        self.k_o = (self.a) / (2 * self.w + self.a)
        self.k_op = np.sqrt(1 - self.k_o ** 2)

        self.k = (np.tanh((np.pi * self.a / 2) / (2 * self.s))) / (
            np.tanh((np.pi * (self.w + self.a / 2)) / (2 * self.s))
        )
        self.k_p = np.sqrt(1 - self.k ** 2)

    def resistance(self, **kargs):
        """Resistance"""
        for key, value in kargs.items():
            if key == "mu_r":
                self.μ = self.μ_o * value
            elif key == "conductivity":
                self.σ = value
            elif key == "width":
                self.w = value
            elif key == "height":
                self.h = value
            elif key == "length":
                self.l = value

        δ = np.sqrt(2 / (self.ω * self.μ * self.σ))
        R_dc = (self.l) / (self.σ * self.w * self.h)
        R_ac = (self.l) / (self.σ * self.w * δ)

        return R_dc + R_ac

    def capacitance(self):
        """Capacitance"""
        q = (
            (1 / 2)
            * (eliK(self.k_p) / eliK(self.k))
            * (eliK(self.k_o) / eliK(self.k_op))
        )
        ε_eff = 1 + (self.ε_sus - 1) * q
        # ε_eff = self.ε_sus / 2

        C = self.ε_o * ε_eff * ((eliK(self.k_op)) / eliK(self.k_o))
        # C = self.ε_o * (self.ε_sus / 2) * ((eliK(self.k_op) * self.l) / eliK(self.k_o))
        C = C + self.ε_a * self.ε_o * (self.h / self.a)

        return C

    def inductance(self):
        """Inductance"""
        # ε = self.ε_o * self.ε_a
        # L = self.μ_o * (2 / (1 + ε)) * ((eliK(self.k_o)) / (eliK(self.k_op)))
        # L = self.μ_o * (ε / 2) * ((eliK(self.k_o)) / (eliK(self.k_op)))
        C = self.ε_o * (self.ε_a / 2) * ((eliK(self.k_op)) / eliK(self.k_o))
        C = C + self.ε_a * self.ε_o * (self.h / self.a)
        # L = self.μ_o * self.ε_o * (1 / C)
        L = self.μ_o * self.ε_o * (1 / C)

        return L

    def conductance(self):
        """Conductance"""
        q = (
            (1 / 2)
            * (eliK(self.k_p) / eliK(self.k))
            * (eliK(self.k_o) / eliK(self.k_op))
        )
        ε_eff = 1 + (self.ε_sus - 1) * q
        G = ((np.sqrt(ε_eff)) / (120 * np.pi)) * ((eliK(self.k_o)) / (eliK(self.k_op)))
        # C = self.capacitance()
        # G = self.ε_o * self.μ_o * (1 / C)

        return G

class Sensor(SlotLine):
    def __init__(self, freq, comb, *args, **kargs):
        self.metal = comb[0]
        self.subs = comb[1]
        self.film = comb[2]
        self.f = freq

        SlotLine.__init__(self, self.f, self.metal, self.subs)

    def impedance(self, f_val, *args):
        R = self.resistance()
        C = self.capacitance()
        L = self.inductance()
        G = self.conductance()
        G = 0

        if f_val:
            args = list(self.film.values())[0]
            args["height"] = 1e-6
            args["length"] = self.a
            args["width"] = self.l
            G_p = 1 / self.resistance(**args)
            G = G + G_p

        z_o = np.sqrt((R + 1j * self.ω * L) / (G + 1j * self.ω * C))
        γ = np.sqrt((R + 1j * self.ω * L) * (G + 1j * self.ω * C))

        z_ref = z_o / np.tanh(γ * self.l)
        # print(z_ref)
        return z_ref

if __name__ == "__main__":
    with open("materials.json") as file:
        data = json.load(file)["materials"]

        # Request just copper and FP4
        metals = [data[0]["metals"][0]]
        subs = [data[1]["substrate"][0]]

        film = data[2]["film"]
        f = np.linspace(50, 2e6, 300)
        for comb in itertools.product(metals, subs, film):
            slot = Sensor(f, comb)
            z_ref = slot.impedance()
            # s = Sensor(slot)

            plt.plot(f, abs(z_ref), label=list(comb[-1].keys())[0])
        plt.grid()
        plt.legend()
        plt.yscale("log")
        plt.show()
