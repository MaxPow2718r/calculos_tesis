"""
Runtime contains functions to plot and compare all the simulations. Most of the
functions use the data extracted from COMSOL. Some use the analytical model
define in the maths module.

Here are the matplotlib configurations to make all the plots equal.

There are several functions that repeats some code. All the functions in this
file could be passed to a class and optimize.

I tried to make docstrings for all the functions but it's better to read the
code and test it to understand better its functionality.
"""
import itertools
import json

from mpl_toolkits.axes_grid1 import host_subplot
from sklearn.linear_model import LinearRegression
from lib.maths import Sensor
from utils.format import data_to_csv
from io import StringIO

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.axisartist as AA

plt.rcParams["svg.fonttype"] = "none"
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 11

from lib.definitions import (
    mat_1,
    mat_2,
    mat_3,
    freq,
    z_ref,
    adm,
    metals,
    subs,
    films,
    combinations,
)


def save_or_show(name):
    if name:
        plt.savefig(name)
    else:
        plt.show()


def plot_diff(df_init, df_end, title, label1, label2, name=""):
    """
    This will plot the difference between curves is configured by default to
    plot just copper and FP4 it has to be changed manually.

    Arguments:
    df_init -- (data frame) data frame of the first plot, probably the system
    without the polymer.
    df_end -- (data frame) final data frame, the one with de polymer.

    Return:
    None
    """

    x_init = []
    y_init = []

    x_end = df_end[0]
    y_end = df_end[1]

    try:
        for i in range(len(df_init)):
            if df_init[mat_1].values[i] == 1 and df_init[mat_2].values[i] == 1:
                x_init.append(df_init[freq].values[i])
                y_init.append(df_init[z_ref].values[i])
                # x_end.append(df_end[freq].values[i])
                # y_end.append(df_end[z_ref].values[i])

            # if (
            #     df_end[mat_1].values[i] == 1
            #     and df_end[mat_2].values[i] == 1
            # ):
    except:
        x_init = df_init[freq]
        y_init = df_init[z_ref]

    plt.plot(x_init, y_init, label=label1)
    plt.plot(x_end, y_end, label=label2)
    plt.legend()
    plt.grid()
    plt.yscale("log")
    plt.title(title)
    plt.tight_layout()
    save_or_show(name)


def filter_df(df, header, comb):
    """
    This function will order all the given information separating it in
    dictionaries by combination.

    Arguments:
    df -- (data frame) The data frame with all the information
    header -- (list) The header of the data frame [not used for now]
    comb -- (tuple) all the possible combinations of materials

    Return:
    info -- a dictionary with the combination and the frequency and impedance.
    """
    metal, sub, film = comb

    info = {"combination": comb}
    x_freq = []
    y_imp = []
    for i in range(len(df[freq])):
        if (
            df[mat_1].values[i] == metals.index(metal) + 1
            and df[mat_2].values[i] == subs.index(sub) + 1
            and df[mat_3].values[i] == films.index(film)
        ):
            x_freq.append(df[freq].values[i])
            y_imp.append(df[imp].values[i])

    d = {freq: x_freq, imp: y_imp}
    info["data_frame"] = pd.DataFrame(data=d)

    return info


def plot_film_diff(df, name=""):
    """
    This function is used to plot all the impedance for the diferent polymer
    films

    Arguments:
    df -- (data frame) pandas dataframe with the data
    name -- (str) The name to save the file
    """
    df_no_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 0)]
    df_pedot = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 1)]
    df_pani = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 2)]
    df_ppy = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 3)]

    fig, ax1 = plt.subplots()

    (p1,) = ax1.plot(
        df_pani[freq], df_pani[z_ref], color="red", label="Sensor con PANI"
    )

    ax2 = ax1.twinx()
    (p2,) = ax2.plot(
        df_pedot[freq], df_pedot[z_ref], color="blue", label="Sensor con PEDOT:PSS"
    )

    ax3 = ax1.twinx()
    (p3,) = ax3.plot(df_ppy[freq], df_ppy[z_ref], color="green", label="Sensor con PPy")

    ax3.spines["right"].set_position(("outward", 70))

    ax1.set_xlabel("Frecuencia [Hz]")
    ax1.set_ylabel("Impedancia $[\Omega]$ PANI", color="red")
    ax2.set_ylabel("Impedancia $[\Omega]$ PEDOT", color="blue")
    ax3.set_ylabel("Impedancia $[\Omega]$ PPy", color="green")

    ax1.grid()
    lns = [p1, p2, p3]
    ax1.legend(handles=lns, loc="best")

    ax1.set_title(
        "Comparación entre impedancias del sensor con diferentes películas de polímero"
    )

    plt.tight_layout()

    save_or_show(name)
    # plt.show()


def plotter(df):
    for num, f in enumerate(films):
        fig, axes = plt.subplots(2, 2)
        title_index = 0
        for ax, comb in zip(axes.flat, combinations):
            for label_index, mat in enumerate(comb):
                x = []
                y = []

                for i in range(len(df[mat_1])):
                    if (
                        df[mat_1].values[i] == mat[0]
                        and df[mat_2].values[i] == mat[1]
                        and df[mat_3].values[i] == num
                    ):
                        x.append(df[freq].values[i])
                        y.append(df[z_ref].values[i])

                ax.plot(x, y, label=subs[label_index])
                ax.set_yscale("log")
                ax.set_xlabel("Frecuency [Hz]")
                ax.set_ylabel("Impedance $[\Omega$]")
            ax.set_title(metals[title_index])
            ax.legend()
            ax.grid(True)
            title_index += 1

        plt.tight_layout()
        name = ""
        save_or_show(name)


def get_film_impedance(df):
    """
    Plots the difference for all the polymers between the sensor without polymer
    and the sensor with polymer

    Arguments:
    df -- (data frame)
    """
    impedance = dict()
    for num, f in enumerate(films[1:]):
        df_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == num + 1)]
        df_no_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 0)]

        diff = [abs(x - y) for x, y in zip(df_film[z_ref], df_no_film[z_ref])]

        fig, ax = plt.subplots()
        plt.subplots_adjust(right=0.80)
        (p1,) = ax.plot(
            df_film[freq], df_film[z_ref], label="Sensor con polímero", color="blue"
        )
        ax.set_xlabel("Frecuencia [Hz]")
        ax.set_ylabel("Impedancia [$\Omega$] del sensor con " + f, color="blue")
        ax.tick_params("y", colors="b")

        ax2 = ax.twinx()
        (p2,) = ax2.plot(
            df_film[freq], df_no_film[z_ref], label="Sensor " + films[0], color="red"
        )
        ax2.set_ylabel("Impedancia [$\Omega$] (Escala logarítmica) ")
        ax2.set_yscale("log")

        (p3,) = ax2.plot(
            df_film[freq], diff, label="Diferencia entre las curvas", color="green"
        )

        lns = [p1, p2, p3]
        ax.legend(handles=lns, loc="best")
        ax.grid()
        ax.set_title(f"Comparación para el sensor con {f}")
        fig.tight_layout()
        plt.show()

        fr = df_film[freq].to_numpy()
        d = {freq: fr, z_ref: diff}
        impedance[f] = pd.DataFrame(data=d)

    return impedance


def plot_vs(df, name=""):
    with open("materials.json") as file:
        data = json.load(file)["materials"]

        metals = data[0]["metals"]
        subs = data[1]["substrate"]
        film = data[2]["film"]
        df_no_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 0)]
        f = df_no_film[freq].to_numpy()
        slot = Sensor(f, (metals[0], subs[0], film[0]))
        z = [abs(x) for x in slot.impedance(0)]
        diff = [(abs(x - y) / y) * 100 for x, y in zip(z, df_no_film[z_ref])]

        plt.plot(f, z, label="Cálculo analítico")
        plt.plot(df_no_film[freq], df_no_film[z_ref], label="Cálculo COMSOL")
        plt.ylabel("Impedancia $[\Omega]$")
        plt.xlabel("Frecuencia [Hz]")
        plt.title("Comparación entre el calculo analítico y la simulación en COMSOL")
        plt.yscale("log")
        plt.grid()
        plt.legend()
        # name = "/home/pablo/Tesis/escrito/images/computos_cobre_fp4_sin_polimero.svg"
        save_or_show(name)


def plot_film_a(df):
    with open("materials.json") as file:
        data = json.load(file)["materials"]

        df_ppy = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 3)]
        df_pani = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 2)]
        df_pedot = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 1)]

        # Request just copper and FP4
        metals = [data[0]["metals"][0]]
        subs = [data[1]["substrate"][0]]

        film = data[2]["film"]
        f = np.linspace(50, 2e6, 300)
        for comb in itertools.product(metals, subs, film):
            slot = Sensor(f, comb)
            z = slot.impedance(1)
            # s = Sensor(slot)

            fig, ax = plt.subplots()
            (p1,) = ax.plot(f, abs(z), color="red", label="Calculo analítico Python")
            ax2 = ax.twinx()
            if list(comb[-1].keys())[0] == "PANI":
                (p2,) = ax2.plot(
                    df_pani[freq],
                    df_pani[z_ref],
                    color="blue",
                    label="Calculo en COMSOL",
                )
                ax.set_title("Comparación de calculos con PANI")
            elif list(comb[-1].keys())[0] == "PEDOT":
                (p2,) = ax2.plot(
                    df_pedot[freq],
                    df_pedot[z_ref],
                    color="blue",
                    label="Calculo en COMSOL",
                )
                ax.set_title("Comparación de calculos con PEDOT")
            elif list(comb[-1].keys())[0] == "PPy":
                (p2,) = ax2.plot(
                    df_ppy[freq], df_ppy[z_ref], color="blue", label="Calculo en COMSOL"
                )
                ax.set_title("Comparación de calculos con PPy")
            ax.set_ylabel("Impedancia $[\Omega]$ Python", color="red")
            ax2.set_ylabel("Impedancia $[\Omega]$ COMSOL", color="blue")
            ax.set_xlabel("Frecuencia [Hz]")
            ax.set_yscale("log")

            lns = [p1, p2]
            ax.legend(handles=lns, loc="best")

            plt.tight_layout()
            ax.grid()
            plt.show()


def draw_inset_pani(df, points):
    """
    This function plots the stimated values for the impedance of PANI film for
    different concentrations of ammonia. It draws the full frequency range and a
    small range in an inset.

    Inputs
    df -- data frame with simulation data
    points -- data frame of the stimated ammonia curve
    """
    df_min = points["x"].min()
    df_max = points["x"].max()
    steps = 100
    x_new = np.linspace(df_min, df_max, steps)
    df_pani = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 2)]

    reg = LinearRegression()
    reg.fit(points[["x"]], points.y)
    ppm = np.array([200, 400, 600, 800, 1000])
    s = reg.predict(ppm.reshape((-1, 1)))
    print("PANI")
    print(ppm)
    print(s)
    fig, ax = plt.subplots()
    ax.plot(df_pani[freq], df_pani[z_ref], label="En aire")
    for res, con in zip(s, ppm):
        label = "{} ppm".format(con)
        ax.plot(df_pani[freq], res * df_pani[z_ref], label=label)

    ax.set_xlabel("Frecuencia [Hz]")
    ax.set_ylabel("Resistencia $[\Omega]$")
    ax.set_title(
        "Comparación entre las resistencias del polímero PANI con diferentes concentraciones de gas de amoniaco"
    )
    ax.grid()
    ax.legend()
    plt.tight_layout()
    plt.show()
    # plt.savefig("/home/pablo/Tesis/escrito/images/pani_diferentes_concentraciones.svg")


def com_ppy_pedot(df):
    """
    Plots concentration graphs for ppy and pedot
    """
    points_pedot = pd.read_csv(
        "extra/seekaew2014/points.csv", index_col=None, names=["x", "y"]
    )
    points_ppy = pd.read_csv(
        "extra/joshi_2011/points.csv", index_col=None, names=["x", "y"]
    )

    # PPy case
    points_ppy = points_ppy.loc[3:10]
    # print(points_ppy)
    df_min = points_ppy["x"].min()
    df_max = points_ppy["x"].max()
    steps = 100
    x_new = np.linspace(df_min, df_max, steps)
    df_ppy = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 3)]

    reg = LinearRegression()
    reg.fit(points_ppy[["x"]], points_ppy.y)
    ppm = np.array([25, 40, 60, 80])
    s = reg.predict(ppm.reshape((-1, 1)))
    print("PPy")
    print(ppm)
    print(s)
    fig, ax = plt.subplots()
    ax.plot(df_ppy[freq], df_ppy[z_ref], label="En aire")
    for res, con in zip(s, ppm):
        label = "{} ppm".format(con)
        ax.plot(df_ppy[freq], ((res / 100) + 1) * df_ppy[z_ref], label=label)

    ax.set_xlabel("Frecuencia [Hz]")
    ax.set_ylabel("Resistencia $[\Omega]$")
    ax.set_title(
        "Comparación entre las resistencias del polímero PPy con diferentes concentraciones de gas de amoniaco"
    )
    ax.grid()
    ax.legend()
    plt.tight_layout()
    plt.show()
    # plt.savefig("/home/pablo/Tesis/escrito/images/pani_diferentes_concentraciones_inset.svg")

    # PEDOT case
    points_pedot_1 = points_pedot.loc[0:4]
    points_pedot_2 = points_pedot.loc[4:7]
    points_pedot_3 = points_pedot.loc[7:8]
    ps = [points_pedot_1, points_pedot_2, points_pedot_3]

    df_min_1 = points_pedot_1["x"].min()
    df_max_1 = points_pedot_1["x"].max()
    df_min_2 = points_pedot_2["x"].min()
    df_max_2 = points_pedot_2["x"].max()
    df_min_3 = points_pedot_3["x"].min()
    df_max_3 = points_pedot_3["x"].max()

    steps = 100
    x_new_1 = np.linspace(df_min_1, df_max_1, steps)
    x_new_2 = np.linspace(df_min_2, df_max_2, steps)
    x_new_3 = np.linspace(df_min_3, df_max_3, steps)

    xs = [x_new_1, x_new_2, x_new_3]

    df_pedot = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 1)]

    reg_1 = LinearRegression()
    reg_2 = LinearRegression()
    reg_3 = LinearRegression()

    reg_1.fit(points_pedot_1[["x"]], points_pedot_1.y)
    reg_2.fit(points_pedot_2[["x"]], points_pedot_2.y)
    reg_3.fit(points_pedot_3[["x"]], points_pedot_3.y)
    regs = [reg_1, reg_2, reg_3]

    ppm_1 = np.array([100])
    ppm_2 = np.array([200, 400, 600])
    ppm_3 = np.array([800, 1000])
    ppms = [ppm_1, ppm_2, ppm_3]

    s_1 = reg_1.predict(ppm_1.reshape((-1, 1)))
    s_2 = reg_2.predict(ppm_2.reshape((-1, 1)))
    s_3 = reg_3.predict(ppm_3.reshape((-1, 1)))
    ss = [s_1, s_2, s_3]

    fig, ax = plt.subplots()
    ax.plot(df_ppy[freq], df_ppy[z_ref], label="En aire")

    print("PEDOT")
    print(ppms)
    print(ss)
    for res, con in zip(s_1, ppm_1):
        label = "{} ppm".format(con)
        ax.plot(df_ppy[freq], ((res / 100) + 1) * df_ppy[z_ref], label=label)
    for res, con in zip(s_2, ppm_2):
        label = "{} ppm".format(con)
        ax.plot(df_ppy[freq], ((res / 100) + 1) * df_ppy[z_ref], label=label)
    for res, con in zip(s_3, ppm_3):
        label = "{} ppm".format(con)
        ax.plot(df_ppy[freq], ((res / 100) + 1) * df_ppy[z_ref], label=label)

    ax.set_xlabel("Frecuencia [Hz]")
    ax.set_ylabel("Resistencia $[\Omega]$")
    ax.set_title(
        "Comparación entre las resistencias del polímero PEDOT con diferentes concentraciones de gas de amoniaco"
    )
    ax.grid()
    ax.legend()
    plt.tight_layout()
    plt.show()

def create_table():
    """
    Prints a latex table formated text to standard output with stimated values
    for conductivity an 20, 40, 60 and 80 ppm.
    """
    df = pd.read_csv("data/all_data.csv")
    points_pani = pd.read_csv(
        "extra/liu_2021/points.csv", index_col=None, names=["x", "y"]
    )
    points_ppy = pd.read_csv(
        "extra/joshi_2011/points.csv", index_col=None, names=["x", "y"]
    )
    points_pedot = pd.read_csv(
        "extra/seekaew2014/points.csv", index_col=None, names=["x", "y"]
    )
    header = list(df.columns)

    points_ppy = points_ppy.loc[3:11]
    points_pedot = points_pedot.loc[0:4]
    # df_min = points["x"].min()
    # df_max = points["x"].max()
    steps = 100

    σ_o_ppy = 7.9
    σ_o_pani = 0.33
    σ_o_pedot = 10

    df_no_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 0)]
    df_ppy = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 3)]
    df_pani = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 2)]
    df_pedot = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 1)]
    ppm = np.array([20, 40, 60, 80])

    reg_ppy = LinearRegression()
    reg_pani = LinearRegression()
    reg_pedot = LinearRegression()
    reg_ppy.fit(points_ppy[["x"]], points_ppy.y)
    reg_pani.fit(points_pani[["x"]], points_pani.y)
    reg_pedot.fit(points_pedot[["x"]], points_pedot.y)

    s_ppy = reg_ppy.predict(ppm.reshape((-1, 1)))
    s_pedot = reg_pedot.predict(ppm.reshape((-1, 1)))
    s_pani = reg_pani.predict(ppm.reshape((-1, 1)))

    σ_ppy = [ σ_o_ppy / (1 + x / 100) for x in s_ppy ]
    σ_pedot = [ σ_o_pedot / (1 + x / 100) for x in s_pedot ]
    σ_pani = [ σ_o_pani / x for x in s_pani ]

    string = [ "$\\sigma$ a {} ppm".format(x) for x in ppm ]
    string = " & ".join(string)
    print("Material", "$\\sigma$ En aire", string, sep=" & ", end="\\\\\n")

    string = [ "{:.03f}".format(x) for x in σ_ppy ]
    string = " & ".join(string)
    print("PPy", "{:.03f}".format(σ_o_ppy), string, sep=" & ", end="\\\\\n")

    string = [ "{:.03f}".format(x) for x in σ_pedot ]
    string = " & ".join(string)
    print("PEDOT", "{:.03f}".format(σ_o_pedot), string, sep=" & ", end="\\\\\n")

    string = [ "{:.03f}".format(x) for x in σ_pani ]
    string = " & ".join(string)
    print("PANI", "{:.03f}".format(σ_o_pani), string, sep=" & ", end="\\\\\n")

def comparative():
    df = pd.read_csv("data/all_data.csv")
    points_pani = pd.read_csv(
        "extra/liu_2021/points.csv", index_col=None, names=["x", "y"]
    )
    points_ppy = pd.read_csv(
        "extra/joshi_2011/points.csv", index_col=None, names=["x", "y"]
    )
    points_pedot = pd.read_csv(
        "extra/seekaew2014/points.csv", index_col=None, names=["x", "y"]
    )
    header = list(df.columns)

    points_ppy = points_ppy.loc[3:10]
    points_pedot_1 = points_pedot.loc[0:4]
    # df_min = points["x"].min()
    # df_max = points["x"].max()
    steps = 100
    # x_new = np.linspace(df_min, df_max, steps)
    df_no_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 0)]
    df_ppy = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 3)]
    df_pani = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 2)]
    df_pedot = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 1)]
    # ppm = np.linspace(20, 80, 5)
    ppm = np.array([20, 40, 60, 80])

    reg_ppy = LinearRegression()
    reg_pani = LinearRegression()
    reg_pedot = LinearRegression()
    reg_ppy.fit(points_ppy[["x"]], points_ppy.y)
    reg_pani.fit(points_pani[["x"]], points_pani.y)
    reg_pedot.fit(points_pedot[["x"]], points_pedot.y)

    s_ppy = reg_ppy.predict(ppm.reshape((-1, 1)))
    s_pedot = reg_pedot.predict(ppm.reshape((-1, 1)))
    # convert this response to be the same as the others
    s_pani = [abs(x - 1) * 100 for x in reg_pani.predict(ppm.reshape((-1, 1)))]

    imp_no_film = df_no_film.loc[df_no_film[freq] == 50][z_ref].values[0]
    imp_pani = df_pani.loc[df_pani[freq] == 50][z_ref].values[0]
    imp_pedot = df_pedot.loc[df_pedot[freq] == 50][z_ref].values[0]
    imp_ppy = df_ppy.loc[df_ppy[freq] == 50][z_ref].values[0]

    r_pani = [(imp_no_film + (1 + x / 100) * (imp_no_film - imp_pani)) for x in s_pani]
    r_pedot = [
        (imp_no_film + (1 + x / 100) * (imp_no_film - imp_pedot)) for x in s_pedot
    ]
    r_ppy = [(imp_no_film + (1 + x / 100) * (imp_no_film - imp_ppy)) for x in s_ppy]

    fig, ax1 = plt.subplots()

    (p1,) = ax1.plot(ppm, r_pani, color="red", label="Sensor con PANI")

    ax2 = ax1.twinx()
    (p2,) = ax2.plot(ppm, r_pedot, color="blue", label="Sensor con PEDOT:PSS")

    # ax3 = ax1.twinx()
    (p3,) = ax1.plot(ppm, r_ppy, color="green", label="Sensor con PPy")

    ax1.set_xlabel("Concentración de amoniaco (ppm)")

    ax1.set_ylabel("Resistencia a 500 [kHz] PANI y PPy en $[\Omega]$")
    ax2.set_ylabel("Resistencia a 500 [kHz] PEDOT en $[\Omega]$")
    # ax3.set_ylabel("Resistencia a 50 [Hz] PPy en $[\Omega]$", color="green")

    # ax3.spines["right"].set_position(("outward", 70))

    ax1.grid()
    lns = [p1, p2, p3]
    ax1.legend(handles=lns, loc="best")

    ax1.set_title("Comparativa entre resistencias del sensor")

    plt.tight_layout()

    plt.show()

    # response S at 500 kHz
    s_ppy = [x / imp_ppy for x in r_ppy]
    s_pedot = [x / imp_pedot for x in r_pedot]
    s_pani = [x / imp_pani for x in r_pani]
    plt.plot(ppm, s_ppy, label="PPy")
    plt.plot(ppm, s_pedot, label="PEDOT")
    plt.plot(ppm, s_pani, label="PANI")
    plt.grid()
    plt.legend()
    plt.xlabel("Concentración (ppm)")
    plt.ylabel("Respuesta S ($\mathrm{R_{gas} / R_{aire}}$)")
    plt.title("Comparación de respuesta de los sensores")
    plt.show()


def comparative_per():
    df = pd.read_csv("data/all_data.csv")
    points_pani = pd.read_csv(
        "extra/liu_2021/points.csv", index_col=None, names=["x", "y"]
    )
    points_ppy = pd.read_csv(
        "extra/joshi_2011/points.csv", index_col=None, names=["x", "y"]
    )
    points_pedot = pd.read_csv(
        "extra/seekaew2014/points.csv", index_col=None, names=["x", "y"]
    )
    header = list(df.columns)

    points_ppy = points_ppy.loc[3:10]
    points_pedot_1 = points_pedot.loc[0:4]
    steps = 100
    df_no_film = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 0)]
    df_ppy = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 3)]
    df_pani = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 2)]
    df_pedot = df.loc[(df[mat_1] == 1) & (df[mat_2] == 1) & (df[mat_3] == 1)]
    ppm = np.linspace(20, 80, 5)

    reg_ppy = LinearRegression()
    reg_pani = LinearRegression()
    reg_pedot = LinearRegression()
    reg_ppy.fit(points_ppy[["x"]], points_ppy.y)
    reg_pani.fit(points_pani[["x"]], points_pani.y)
    reg_pedot.fit(points_pedot[["x"]], points_pedot.y)

    # % response to ammonia
    s_ppy = reg_ppy.predict(ppm.reshape((-1, 1)))
    s_pedot = reg_pedot.predict(ppm.reshape((-1, 1)))
    # convert this response to be the same as the others
    s_pani = [abs(x - 1) * 100 for x in reg_pani.predict(ppm.reshape((-1, 1)))]

    fig, ax1 = plt.subplots()

    (p1,) = ax1.plot(ppm, s_pani, color="red", label="Sensor con PANI")

    # ax2 = ax1.twinx()
    (p2,) = ax1.plot(ppm, s_pedot, color="blue", label="Sensor con PEDOT:PSS")

    (p3,) = ax1.plot(ppm, s_ppy, color="green", label="Sensor con PPy")

    ax1.set_xlabel("Concentración de amoniaco (ppm)")

    ax1.set_ylabel("Respuesta porcentual a 500 [kHz]")
    ax1.grid()
    lns = [p1, p2, p3]
    ax1.legend(handles=lns, loc="best")

    ax1.set_title("Comparativa entre las respuestas del sensor")

    plt.tight_layout()

    plt.show()

def alternatives():
    """
    Compares the data for different lengths
    """
    (header, csv) = data_to_csv("data/alt_gas.txt")
    df_ppy = pd.read_csv(StringIO(csv), index_col=None, names=header)

    ls = ["l3", "l6", "l10", "l12"]
    names = ["3 mm", "6 mm", "10 mm", "12 mm"]

    conc = ["20 ppm", "40 ppm", "60 ppm", "80 ppm"]

    print("\\begin{table}")
    print("\\centering")
    print("\\caption[Comparación entre diferentes largos]{caption}")
    print("\\label{table:comp_largos}")
    print("\\begin{tabular}{lrrrr}")
    print("\\hline")
    print(" & \\multicolumn{4}{c}{Concentración}", end=" \\\\\n")
    print("Largo", end=" & ")
    print(" & ".join(conc), end=" \\\\\n")
    print("\\hline")
    print("\\hline")
    # iterates the data for every concentration at 500 kHz
    for l, n in zip(ls, names):
        z_a = df_ppy.loc[(df_ppy["conc"] == 1) & (df_ppy[freq] == 500000)][l].values[0]
        print(n, end=" & ")
        S = []
        for i, con in enumerate(conc):
            z_g = df_ppy.loc[(df_ppy["conc"] == i + 2) & (df_ppy[freq] == 500000)][l].values[0]

            S.append(z_g / z_a)
        string = [ "{:.03f}".format(x) for x in S ]
        print(" & ".join(string), end=" \\\\\n")
    print("\\hline")
    print("\\end{tabular}")
    print("\\end{table}")


def gas_response():
    """
    plots the impedance vs frequency of all polymers simulated in COMSOL for
    different ammonia gas concentrations.
    """
    (header, csv) = data_to_csv("data/chamber_gas_for_copper_and_fp4.txt")
    df = pd.read_csv(StringIO(csv), index_col=None, names=header)

    for i in range(1, 4):
        df_mat = df.loc[df[mat_3] == i]

        # plots for all frequency
        fig, ax = plt.subplots()
        for con in ["en aire", "20 ppm", "40 ppm", "60 ppm", "80 ppm"]:
            ax.plot(df_mat[freq], df_mat[con], label=con)

        # plots inset
        left, bottom, width, height = [0.25, 0.25, 0.3, 0.3]
        ax2 = fig.add_axes([left, bottom, width, height])

        for con in ["en aire", "20 ppm", "40 ppm", "60 ppm", "80 ppm"]:
            ax2.plot(df_mat[freq].to_numpy()[:100], df_mat[con].to_numpy()[:100], label=con)


        ax.set_xlabel("Frecuencia [Hz]")
        ax.set_ylabel("Impedancia [$\Omega$]")
        ax2.set_xlabel("Frecuencia [Hz]")
        ax2.set_ylabel("Impedancia [$\Omega$]")

        ax.grid()
        ax2.grid()

        # ax2.yaxis.tick_right()
        # ax2.yaxis.set_label_position("right")

        ax.legend()
        # ax.set_yscale("log")
        ax.set_title("Comparación de diferentes concentraciones {}".format(films[i]))
        plt.tight_layout()
        plt.show()

def response_table():
    """
    Creates a latex table with response and sensitivity for different
    concentrations
    """
    (header, csv) = data_to_csv("data/chamber_gas_for_copper_and_fp4.txt")
    df_gas = pd.read_csv(StringIO(csv), index_col=None, names=header)

    frequency = [50, 50000, 500000, 2000000]

    print(
        """\\begin{table}
\\centering
\\caption[Comparación de sensibilidad entren sensores a diferentes
frecuencias]{Comparación entre las sensibilidades de los sensores con diferentes
	polímeros. La sensibilidad $b_i$ se calcula utilizando la ecuación
	\\ref{eq:sensibilidad}. Dado que las respuestas de los sensores están
	dados por regresiones lineales, la respuesta será lineal y la
	sensibilidad se interpreta la pendiente de dicha recta.}
\\label{table:com_sensibilidad}
\\begin{tabular}{llrrr}
\\hline
& Polímero & Respuesta a $80 \\si{ppm}$ & Respuesta a $20 \\si{ppm}$ & Sensibilidad [\\si{pmm^{-1}}] \\\\
\\hline
\\hline"""
    )
    for f in frequency:
        print("\\multirow{3}{*}{$", f, " \\si{\\hertz}$}", sep="", end=" &")
        for i in range(1, 4):
            df_mat = df_gas.loc[df_gas[mat_3] == i]

            con = ["en aire", "20 ppm", "80 ppm"]
            imp_20 = df_mat.loc[df_mat[freq] == f]["20 ppm"].values[0]
            imp_80 = df_mat.loc[df_mat[freq] == f]["80 ppm"].values[0]
            imp_air = df_mat.loc[df_mat[freq] == f]["en aire"].values[0]
            S_20 = imp_20 / imp_air
            S_80 = imp_80 / imp_air

            b = (S_80 - S_20) / (80 - 20)

            if i == 1:
                print(
                    films[i],
                    "{:.03f}".format(S_80),
                    "{:.03f}".format(S_20),
                    "{:.03e}".format(b),
                    sep=" & ",
                    end=" \\\\\n",
                )
            else:
                print(
                    "",
                    films[i],
                    "{:.03f}".format(S_80),
                    "{:.03f}".format(S_20),
                    "{:.03e}".format(b),
                    sep=" & ",
                    end=" \\\\\n",
                )
        print("\\hline")

    print(
        """\\hline
\\end{tabular}
\\end{table}"""
    )

def compare_conc_and_freq():
    """
    This function plots graphs to compare how the response of the sensor changes
    with different concentrations at different frequencies.
    """
    (header, csv) = data_to_csv("data/chamber_gas_for_copper_and_fp4.txt")
    df_gas = pd.read_csv(StringIO(csv), index_col=None, names=header)

    frequency = [50, 50000, 500000, 2000000]
    # frequency = [50]
    for f in frequency:
        for i in range(1, 4):
            df_mat = df_gas.loc[df_gas[mat_3] == i]
            imp_air = df_mat.loc[df_mat[freq] == f]["en aire"].values[0]

            S = []
            for j in ["20 ppm", "40 ppm", "60 ppm", "80 ppm"]:
                imp = df_mat.loc[df_mat[freq] == f][j].values[0]
                S.append(imp / imp_air)

            plt.plot([20, 40, 60, 80], S, "o-", label=films[i])

        plt.grid()
        plt.legend()
        plt.xlabel("Concentración en ppm")
        plt.ylabel("Respuesta $\mathrm{R_{gas} / R_{aire}}$")
        plt.title("Comparación de respuesta entre polímeros a {} Hz".format(f))
        plt.tight_layout()
        plt.show()


    frequency = [50, 50000, 500000, 2000000]
    for f in frequency:
        df_mat = df_gas.loc[df_gas[mat_3] == 3]
        imp_air = df_mat.loc[df_mat[freq] == f]["en aire"].values[0]

        S = []
        for j in ["20 ppm", "40 ppm", "60 ppm", "80 ppm"]:
            imp = df_mat.loc[df_mat[freq] == f][j].values[0]
            S.append(imp / imp_air)

        plt.plot([20, 40, 60, 80], S, "o-", label=f"{f} Hz")

    plt.grid()
    plt.legend()
    plt.xlabel("Concentración en ppm")
    plt.ylabel("Respuesta $\mathrm{R_{gas} / R_{aire}}$")
    plt.title("Comparación de respuesta para PPy a diferentes frecuencias")
    plt.tight_layout()
    plt.show()
