import os
import json
import itertools

import numpy as np
import pandas as pd
import lib.runtime as rt
import utils.format as fm


from io import StringIO
from lib.maths import SlotLine, Sensor
from lib.definitions import (
    mat_1,
    mat_2,
    mat_3,
    freq,
    z_ref,
    adm,
    metals,
    subs,
    films,
    combinations,
)

import matplotlib.pyplot as plt

from lib.definitions import metals, subs, films

if __name__ == "__main__":
    df = pd.read_csv("data/all_data.csv")
    # points for PANI
    points = pd.read_csv("extra/liu_2021/points.csv", index_col=None, names=["x", "y"])
    header = list(df.columns)

    # rt.plot_film_diff(df, "/home/pablo/Tesis/escrito/images/comparacion_impedancias_chamber.svg")
    # rt.plotter(df)

    # rt.draw_inset_pani(df, points)

    # impedance = rt.get_film_impedance(df)
    # print(impedance)

    # combinatios = []

    # rt.plot_vs(df)

    # rt.draw_inset_pani(df, points)
    # rt.com_ppy_pedot(df)

    # rt.comparative()
    # rt.comparative_per()
    rt.alternatives()

    # rt.plot_film_a(df)
    # rt.create_table()
    # rt.gas_response()
    # rt.response_table()
    # rt.compare_conc_and_freq()
