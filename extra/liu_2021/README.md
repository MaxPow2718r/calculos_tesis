# Curve and points

Information extracte from [Liu's work from
2021](https://iopscience.iop.org/article/10.1149/2162-8777/abe3ce/meta). The
data was extracted from a plot in their work using the [Web PlotDigitalizer
tool](https://apps.automeris.io/wpd/).

![Response of a PANI film to different ammonia concentration](respuesta_liu.png)
