# Curve and points

Information extracte from [Seekaew's work from
2014](https://doi.org/10.1016/j.orgel.2014.08.044). The data was extracted from
a plot in their work using the [Web PlotDigitalizer
tool](https://apps.automeris.io/wpd/).

Modifications were made to the original images to left only the ammonia
response.

![Response of a PEDOT:PSS film to different ammonia
concentration](respuesta_seekaew.png)
