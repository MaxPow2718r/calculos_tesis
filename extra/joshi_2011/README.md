# Curve and points

Information extracte from [Joshi's work from
2011](https://doi.org/10.1016/j.snb.2011.03.009). The data was extracted from
a plot in their work using the [Web PlotDigitalizer
tool](https://apps.automeris.io/wpd/).

![Response of a PPy film to different ammonia concentration](respuesta_joshi.png)
