# Description

This is a short program to calculate some variables corresponding to a gas
sensor for ammonia. The program computes the impedance for the sensor. A C
module was added for formatting the outputs of a different program and correctly
plotted it with [matplotlib](https://matplotlib.org/).

## Usage

To use format the files first compile the program

```console
cd utils
make
```

Then `cat` the file into the program (it takes it as standard input)

```console
cat [unformated/file/path] | ./format > [formated/file/path]
```

To use the `Python` program, first install the dependencies.

```console
pip install -r requirements.txt
```
Then edit the `main.py` file with the needed output and run the program.

```console
python src/main.py
```

## TODO

- [ ] Use the C program as a python library.
- [ ] Make the main an interface.
